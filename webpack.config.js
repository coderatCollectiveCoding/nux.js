module.exports = {
  entry: __dirname + '/src/nux.js',
  output: {
    path: __dirname + '/dist',
    publicPath: '/dist/',
    filename: 'nux.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: 'babel-loader'
    }]
  }
};