export default class TreeItem {
  constructor({
    id,
    div,
    statement,
    group,
    connections,
    tree
  }) {
    this.id = id;
    this.div = div;
    this.statement = statement;
    this.choices = this.connectionsToChoices({
      connections: connections,
      tree: tree
    });
    this.isRoot = group == "rootStatement" ? true : false
  }

  // Assign choices from connections and nest with proper format inside treeItem
  connectionsToChoices({
    connections,
    tree
  }) {
    let choiceItems = [];

    connections.forEach(choiceId => {
      let choice = tree.choices().find(choice => choice.id == choiceId);
      choiceItems.push({
        "text": choice.label,
        "nextItemId": parseInt(choice.connections[0])
      });
    })
    return choiceItems;
  }

  // Text to HTML + replace \n in JSON with <br>s
  nl2br(text) {
    return text.replace(new RegExp("\n", "g"), "<br>");
  }

  // Render self
  renderSelf() {
    const delayTime = 50;

    // Draw statement
    const statement = this.nl2br(this.statement);

    document.getElementsByClassName('statement-text')[0].innerHTML = statement;

    // Remove all choices
    document.querySelectorAll('.choice').forEach(function(choice) {
      choice.parentNode.removeChild(choice)
    });

    // Draw choices
    let choiceTemplate = document.getElementsByClassName('choice-template')[0];
    this.choices.forEach((choice) => {
      const choiceDiv = choiceTemplate.cloneNode(true);
      choiceDiv.className = 'choice';

      const choiceText = this.nl2br(choice.text);

      choiceDiv.setAttribute('next-item-id', choice.nextItemId);
      choiceDiv.innerHTML = choiceText;
      document.querySelector(this.div).appendChild(choiceDiv)
    });
  }
}