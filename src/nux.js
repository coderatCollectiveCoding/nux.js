import TreeItem from './tree_item.js';

window['Nux'] = class Nux {
  constructor(data, div) {
    this.div = div;
    this.createDOM(div);
    this.nodes = data['data'];
    this.removeParentReferences(this.rootNode());
    this.treeItems = this.treeItemFormat();
    this.resetTree(div);
    this.enableButtons();
  }

  createDOM(div) {
    document.querySelector(div).innerHTML = `
      <div class="button-row">
        <button class="back-button" >back</button>
        <button class="reset-button">reset</button>
      </div>
      <div class="statement">
        <h1 class="statement-text"></h1>
      </div>
      <div class="choices">
        <div class="choice-template"></div>
      </div>
    `;
  }

  enableButtons() {
    const that = this;

    document.addEventListener('click', function(e) {
      const element = e.target || e.srcElement; // behavior depending on browser
      if (element.classList.contains('choice')) {
        const nextItemId = element.getAttribute('next-item-id');
        const nextItem = that.getItem(nextItemId);
        that.goToItem(element, nextItem);
      } else if (element.classList.contains('back-button')) {
        if (that.history.length > 0) that.goBack();
      } else if (element.classList.contains('reset-button')) {
        if (that.history.length > 0) that.resetTree();
      };
    });
  }

  // Returns root node element
  rootNode() {
    return this.nodes.find(treeItem => treeItem.group == "rootStatement");
  }

  // Returns root tree item
  rootItem() {
    return this.treeItems.find(treeItem => treeItem.isRoot);
  }

  // Return index of first node with id == nodeId
  indexOfNode(nodeId) {
    return this.nodes.findIndex(node => {
      return node.id == nodeId
    });
  }

  // Return all statement nodes
  statements(data) {
    return this.nodes.filter(node => node.group == "statement" || node.group == "rootStatement");
  }

  // Return all choice nodes
  choices(data) {
    return this.nodes.filter(node => node.group == "choice");
  }

  // Receive arbitrary tree item
  getItem(id) {
    return this.treeItems.find(treeItem => treeItem.id == id);
  }

  // Set currentItem to root and empty history
  resetTree() {
    this.currentItem = this.rootItem();
    this.currentItem.renderSelf();
    this.history = [];
  }

  // Set currentItem to last item in history
  goBack() {
    let previousItem = this.history[this.history.length - 1];
    this.currentItem = previousItem;
    this.currentItem.renderSelf();
    this.history.pop(previousItem);
  }

  // Set currentItem
  goToItem(fromChoice, toItem) {
    this.history.push(this.currentItem);
    this.currentItem = toItem;
    toItem.renderSelf();
  }

  // Attention: Alters 'this' (-> tree instance)
  removeParentReferences(currentNode) {
    currentNode.connections.forEach(nextNodeId => {
      // Fetch next node index
      const nextNodeIndex = this.indexOfNode(nextNodeId);

      // Remove currentNode.id (-> parent) in next nodes and recall func as next currentNode
      const index = this.nodes[nextNodeIndex].connections.findIndex((id) => {
        return id == currentNode.id || id == parseInt(currentNode.id)
      });
      if (index !== -1) this.nodes[nextNodeIndex].connections.splice(index, 1);

      // Recall function
      this.removeParentReferences(this.nodes[nextNodeIndex]);
    })
  }

  // Convert raw nux-builder JSON to treeItem-format
  treeItemFormat() {
    let treeItems = [];
    this.statements().forEach(statement => {
      const newItem = new TreeItem({
        id: statement.id,
        div: this.div,
        statement: statement.label,
        group: statement.group,
        connections: statement.connections,
        tree: this
      });
      treeItems.push(newItem);
    });
    return treeItems;
  }
}