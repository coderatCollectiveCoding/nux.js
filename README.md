# nux.js
nux.js is a leightweight javascript library which easily lets you implement a decision finder based on a question-answer scheme.

### Demo and nux builder
Check out our demo and create your own question-answer dataset on [our nux website](about:blank).

### Usage
Like most javascript libraries simply require one of the library files in your project. Nux then can be initialized via

`new Nux(data, html-identifier)`

where `data` must refer to a valid dataset as defined below and `html-identifier` should point to an existing html element i.e. `'#nux-container'`.

### Data
A valid dataset holds an array of statements and choices as shown in the example below. In creating such a dataset, the following rules have to be followed:
* There must be one and only one rootStatement
* Each choice has to be connected to two statements
* Statements may only be connected to choices

Since it is not trivial to create a valid and reasonable dataset as a plain javascript object, we hardly recommend our [nux builder tool](about:blank). 


```
{"data": [
  {
    "id": "1",
    "connections": [2],
    "label": "You have a nice walk through the forest...",
    "group": "statement"
  },
  {
    "id": "2",
    "connections": [1,3],
    "label": "Left",
    "group": "choice"
  },
  {
    "id": "3",
    "connections": [2,4],
    "label": "Where do you want to walk?",
    "group": "rootStatement"
  },
  {
    "id": "4",
    "connections": [3,5],
    "label": "Right",
    "group": "choice"
  },
  {
    "id": "5",
    "connections": [4,6,7],
    "label": "A monster appears. What do you want to do?",
    "group": "statement"
  },
  {
    "id": "6",
    "connections": [5,8],
    "label": "Fight!",
    "group": "choice"
  },
  {
    "id": "7",
    "connections": [5,9],
    "label": "Run away!",
    "group": "choice"
  },
  {
    "id": "8",
    "connections": [6],
    "label": "You have a good day. The monster is afraid of your fighting skills and runs away...",
    "group": "statement"
  },
  {
    "id": "9",
    "connections": [7],
    "label": "The monster is faster and eats you...",
    "group": "statement"
  }
]};
```

# Development/Modification
The src files are written in ES6, thus not compatible with all browsers.
Therefore they has been transpiled with babel via webpack.

To deliver your changes on the src files as a new dist, install requirements with `npm install` and then run

* `npm run watch` to only watch for/recompile on changes.
* `npm run build` to generate a minified, production-ready build.
